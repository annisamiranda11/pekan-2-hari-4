<?php

    require_once('animal.php');
    require_once('Frog.php');
    require_once('Ape.php');

    $sheep = new Animal('shaun');
    echo "Name : ".$sheep->name;
    echo "<br> Legs : ".$sheep->legs;
    echo "<br> Cold Blooded : ".$sheep->cold_blooded;
    echo "<br><br>";

    $kodok = new Frog("buduk");
    echo "Name : ".$kodok->name;
    echo "<br> Legs : ".$kodok->legs;
    echo "<br> Cold Blooded : ".$kodok->cold_blooded;
    $kodok->jump();

    $sungokong = new Ape("kera sakti");
    echo "<br><br>Name : ".$sungokong->name;
    echo "<br> Legs : ".$sungokong->legs;
    echo "<br> Cold Blooded : ".$sungokong->cold_blooded;
    $sungokong->yell();

?>  